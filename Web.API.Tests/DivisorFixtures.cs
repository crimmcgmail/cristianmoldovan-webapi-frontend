using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.API.Controllers;

namespace Web.API.Tests
{
    [TestClass]
    public class DivisorFixtures
    {
        #region Cristian Test Methods
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void When_x_Is_1_And_nth_Is_minus_1_Then_Throw_Exception_NotSpecified()
        {
            var nthNumber = new DivisorController().Get(1, -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void When_x_Is_0_And_nth_Is_1_Then_Throw_Exception_NotSpecified()
        {
            var nthNumber = new DivisorController().Get(0, 1);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void When_x_Is_2_And_nth_Is_1_Then_Throw_Exception_NotSpecified()
        {
            var nthNumber = new DivisorController().Get(2, 1);
        }

        [TestMethod]
        public void When_x_Is_1_And_nth_Is_1_Then_Result_is_1()
        {
            var nthNumber = new DivisorController().Get(1, 1);
            Assert.AreEqual(1, nthNumber);
        }
        [TestMethod]
        public void When_x_Is_1_And_nth_Is_2_Then_Result_is_1()
        {
            var nthNumber = new DivisorController().Get(1, 2);
            Assert.AreEqual(1, nthNumber);
        }
        [TestMethod]
        public void When_x_Is_1_And_nth_Is_3_Then_Result_is_1()
        {
            var nthNumber = new DivisorController().Get(1, 3);
            Assert.AreEqual(1, nthNumber);
        }
        [TestMethod]
        public void When_x_Is_1_And_nth_Is_4_Then_Result_is_3()
        {
            var nthNumber = new DivisorController().Get(1, 4);
            Assert.AreEqual(3, nthNumber);
        }
        [TestMethod]
        public void When_x_Is_1_And_nth_Is_5_Then_Result_is_5()
        {
            var nthNumber = new DivisorController().Get(1, 5);
            Assert.AreEqual(5, nthNumber);
        }
        [TestMethod]
        public void When_x_Is_1_And_nth_Is_6_Then_Result_is_9()
        {
            var nthNumber = new DivisorController().Get(1, 6);
            Assert.AreEqual(9, nthNumber);
        }
        #endregion Cristian Test Methods

        [TestMethod]
        public void When_x_Is_3_And_nth_Is_1_Then_Result_is_3()
        {
            var nthNumber = new DivisorController().Get(3, 1);
            Assert.AreEqual(3, nthNumber);
        }

        [TestMethod]
        public void When_x_Is_3_And_nth_Is_2_Then_Result_is_9()
        {
            var nthNumber = new DivisorController().Get(3, 2);
            Assert.AreEqual(9, nthNumber);
        }

        [TestMethod]
        public void When_x_Is_5_And_nth_Is_1_Then_Result_is_5()
        {
            var nthNumber = new DivisorController().Get(5, 1);
            Assert.AreEqual(5, nthNumber);
        }

        [TestMethod]
        public void When_x_Is_5_And_nth_Is_2_Then_Result_is_105()
        {
            var nthNumber = new DivisorController().Get(5, 2);
            Assert.AreEqual(105, nthNumber);
        }
    }
}
