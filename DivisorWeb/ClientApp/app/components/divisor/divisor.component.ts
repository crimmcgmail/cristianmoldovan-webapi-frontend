import { Component, Inject } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';

@Component({
    selector: 'fetchdata',
    templateUrl: './divisor.component.html'
})
export class DivisorComponent {
    public result: 0;
    public divideBy: 0;
    public nth: 0;
    private httpService: Http;
    private serviceUrl: string;

    constructor(http: Http, @Inject('API_BASE_URL') baseUrl: string) {
        this.httpService = http;
        this.serviceUrl = baseUrl;
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    public compute() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        
        var params = { x: this.divideBy, nth: this.nth };

        var url = this.serviceUrl + '/divisor';

        this.httpService
            .get(url, { headers: headers, params: params })
            .subscribe(
                result => this.result = result.json(),
                error => console.log("error", error)
            );
    }
}
