﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Controllers
{
    [Route("api/[controller]")]
    public class DivisorController : Controller
    {
        private static readonly int MAX_LOOP_COUNT = 9999;
        /// <summary>
        /// Given the set [1, 1, 1, 3, 5, 9] complete the sequence to 11 numbers
        /// Get the nTH instance of the sequence where (n / x) has no remainder
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nth">the nth count</param>
        /// <returns></returns>
        [HttpGet]
        public int Get(int x, int nth)
        {
            if (nth == 0) throw new ArgumentException($"Invalid divide by value (divide by: {nth})");
            if (x <= 0) throw new ArgumentException($"Invalid field index provided (nth: {x})");
            if (x == 1 && (nth == 1 || nth == 2 || nth == 3)) return 1;

            return ComputeIntegerThreeFibonacci(x, nth);
        }

        private int ComputeIntegerThreeFibonacci(int divideBy, int requestedCount)
        {
            int foundCount = (divideBy == 1? 3: 0);
            var loopCount = 0;
            int a = 1;
            int b = 1;
            int c = 1;
            
            while(true)
            {
                if (++loopCount >= MAX_LOOP_COUNT)
                {
                    throw new ArgumentException($"Executed too many times (divide by: {divideBy}, nth: {requestedCount})");
                }

                int tempa = a;
                int tempb = b;
                a = b;
                b = c;
                c = tempa + tempb + c;

                if (c % divideBy == 0 && (++foundCount) == requestedCount) return c;
            }
        }
    }
}
